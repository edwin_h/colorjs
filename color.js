var Color = function(colorName){
	this.hue = 0;
	this.saturation = 50; 
	this.lightness = 50; 
	this.opacity = 100;

	var randomColor = function(){
		this.hue = Math.floor(Math.random() * (255 - 0 + 1)) + 0;
		this.saturation = Math.floor(Math.random() * 100);
		this.lightness = Math.floor(Math.random() * 100);
		this.opacity = 100;
	}

	var validate = function(){
		if(this.hue > 255){
			this.hue -= 255;
		}else if(this.hue < 0){
			this.hue += 255;
		}
		if(this.saturation > 100){
			this.saturation = 100;
		}else if(this.saturation < 0){
			this.saturation = 0;
		}
		if(this.lightness > 100){
			this.lightness = 100;
		}else if(this.lightness < 0){
			this.lightness = 0;
		}
		if(this.opacity > 100){
			this.opacity = 100;
		}else if(this.opacity < 0){
			this.opacity = 0;
		}
	}

	if(colorName){
		switch(colorName){
			case "red":
				hue = 0;
				saturation = 100;
				lightness = 50;
				break;
			case "blue":
				hue = 240;
				saturation = 100;
				lightness = 50;
				break;
			case "green":
				hue = 120;
				saturation = 100;
				lightness = 50;
				break;
			case "black":
				hue = 0;
				lightness = 0;
				saturation = 0;
				break;
			case "white":
				hue = 0;
				lightness = 100;
				saturation = 0;
				break;
			case "gray":
				hue = 0;
				lightness = 50;
				saturation = 0;
				break;
			default:
				console.log("ColorJS: Didn't recognize color name '"+colorName+"'. Here's a random color instead.");
				randomColor();
				break;
		}
	}
}
Color.prototype.setHue = function(h){
	hue = h;
}
Color.prototype.setSaturation = function(s){
	saturation = s;
}
Color.prototype.setLightness = function(l){
	lightness = l;
}
Color.prototype.setOpacity = function(o){
	opacity = o;
}
Color.prototype.getValue = function(format){
	if(format === "hsl"){
		return "hsl("+hue+","+saturation+"%,"+lightness+"%)";
	}else if(format === "hsla"){
		return "hsla("+hue+","+saturation+"%,"+lightness+"%,"+opacity*0.01+")";
	}
}
Color.prototype.lighten = function(amount){
	lightness += amount;
}
Color.prototype.darken = function(amount){
	lightness -= amount;
}
Color.prototype.saturate = function(amount){
	saturation += amount;
}
Color.prototype.desaturate = function(amount){
	saturation -= amount;
}
Color.prototype.spin = function(amount){
	hue += amount;
}

var color = new Color("purple");
console.log(color);
console.log(color.getValue("hsla"));