# ColorJS

A simple color utility for Javascript. It can validate colors, and generate tidy hsl or hsla values. 

## Usage

Create a new color: 
    myColor = new Color();

Set some values:
    myColor.setHue(200);		// 0-255
    myColor.setSaturation(25);	// 0-100%
    myColor.setLightness(67);	// 0-100%
    myColor.setOpacity(100);	// 0-100%

Generate a color value:
    myColor.getValue("hsla");
    // hsla(200,25%,67%,1)